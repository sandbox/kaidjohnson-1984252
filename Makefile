TARGET = $(filter-out $@,$(MAKECMDGOALS))

.PHONY: $(MAKECMDGOALS)

$(TARGET):
	@if [ $@ = "focus" ]; \
	then \
		scripts/build.sh $(TARGET); \
	fi
	@if [ $@ = "update" ]; \
	then \
		scripts/update.sh $(TARGET); \
	fi
	@if [ $@ = "local" ]; \
	then \
		scripts/local.sh $(TARGET); \
	fi
	@if [ $@ = "webdav" ]; \
	then \
		scripts/webdav.sh $(TARGET); \
	fi
