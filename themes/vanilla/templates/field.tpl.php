<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?></div>
  <?php endif; ?>
  <div class="<?php print $items_classes; ?>"<?php print $content_attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
      <<?php print $item_element; ?> class="<?php print $item_classes; ?> <?php print ($delta % 2) ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>><?php print render($item); ?></<?php print $item_element; ?>>
    <?php endforeach; ?>
  </div>
</div>
