<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see http://drupal.org/node/1728096
 */

define('VANILLA_PATH', drupal_get_path('theme', 'vanilla'));
require_once(dirname(__FILE__) . '/includes/helpers.inc');
require_once(dirname(__FILE__) . '/includes/libraries.inc');

/**
 * Implements hook_css_alter().
 * @see https://www.drupal.org/node/2289623
 */
function vanilla_css_alter(&$css) {
  $styles = array_keys($css);
  $matches = preg_grep('/system\.(menus|theme|messages)\.css$/', $styles);

  foreach ($matches as $key) {
    unset($css[$key]);
  }
}

/**
 * Implements hook_preprocess_html().
 */
function vanilla_preprocess_html(&$vars) {
  drupal_add_js(VANILLA_PATH . '/js/vanilla.js');
  drupal_add_js(VANILLA_PATH . '/js/forms.js');

  // The following scripts are mostly IE dependent.
  // @see http://stackoverflow.com/questions/3855294/html5shiv-vs-dean-edwards-ie7-js-vs-modernizr-which-to-choose

  if (module_exists('libraries')) {
    // For some reason, libraries_detect needs to be run first to get the
    // load to work persistently.
    $library = libraries_detect('console.log');
    if (!empty($library['installed'])) {
      libraries_load('console.log');
    }

    $library = libraries_detect('jquery.custom-file-input');
    if (!empty($library['installed'])) {
      libraries_load('jquery.custom-file-input');
    }

    $library = libraries_detect('respondjs');
    if (!empty($library['installed']) && !empty($vars['add_respond_js'])) {
      libraries_load('respondjs');
    }

    $library = libraries_detect('html5shiv');
    if (!empty($library['installed']) && !empty($vars['add_html5_shim'])) {
      libraries_load('html5shiv');
    }
  }

  // Add colorbox modifications.
  if (module_exists('colorbox')) {
    drupal_add_js(VANILLA_PATH . '/js/colorbox.js', array(
      'scope' => 'footer',
      'weight' => 100,
    ));
  }

  if (module_exists('context')) {
    if ($contexts = context_active_contexts()) {
      foreach ($contexts as $context_name => $context_info) {
        // Add contextual class to body.
        $vars['classes_array'][] = 'context-' . drupal_html_class($context_name);

        // Add contextual layout css.
        if (_vanilla_add_css($context_name)) {
          $added = TRUE;
        }
      }

      // If no matches, try adding a generic layout based on contexts.
      if (empty($added)) {
        foreach ($contexts as $context_name => $context_info) {
          if (FALSE === strpos($context_name, '_')) continue;

          $parts = explode('_', $context_name);
          $part  = reset($parts);
          if (_vanilla_add_css($part)) {
            $added = TRUE;
            break;
          }
        }

        if (empty($added)) {
          _vanilla_add_css('default');
        }
      }
    }
  }

  if ($detect = _vanilla_mobile_detect()) {
    if ($detect->isMobile()) {
      $vars['classes_array'][] = 'is-mobile';
    }

    if ($detect->isTablet()) {
      $vars['classes_array'][] = 'is-tablet';
    }
  }
}

/**
 * Implements hook_preprocess_region().
 */
function vanilla_preprocess_region(&$vars) {
  $vars['tagname'] = 'div';
}

/**
 * Implements hook_preprocess_page().
 */
function vanilla_preprocess_page(&$vars) {
  if (isset($vars['node']) && $vars['node']->status == 0) {
    $vars['classes_array'][] = 'unpublished';
  }

  if (!empty($vars['node']->type)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
}

/**
 * Implements hook_process_page().
 */
function vanilla_process_page(&$vars) {
  if ($separator = filter_xss_admin(theme_get_setting('zen_breadcrumb_separator'))) {
    $vars['breadcrumb'] = preg_replace("#({$separator})#", '<span class="separator">$1</span>', $vars['breadcrumb']);
  }
}

/**
 * Implements hook_preprocess_field().
 */
function vanilla_preprocess_field(&$vars) {
  $vars['items_classes_array'] = array('field-items');
  $vars['item_classes_array'] = array('field-item');
}

/**
 * Implements hook_process_field().
 */
function vanilla_process_field(&$vars) {
  if (empty($vars['item_element'])) {
    $vars['item_element'] = 'div';
  }

  $vars['items_classes'] = implode(' ', $vars['items_classes_array']);
  $vars['item_classes'] = implode(' ', $vars['item_classes_array']);
}

/**
 * Implements hook_preprocess_block().
 */
function vanilla_preprocess_block(&$vars) {
  if ($vars['block']->module == 'views') {
    $hashes = variable_get('views_block_hashes', array());

    // Ensure all views blocks have a non-hashed id attribute.
    if (!empty($hashes[$vars['block']->delta])) {
      $delta = (strtr($hashes[$vars['block']->delta], '_', '-'));
      $vars['attributes_array']['id'] = 'block-views-' . $delta;
      $vars['block_html_id'] = 'block-views-' . $delta;
    }
  }

  // Remove redundant class added by Zen.
  if (in_array('block__title', $vars['title_attributes_array']['class'])) {
    $index = array_search('block__title', $vars['title_attributes_array']['class']);
    unset($vars['title_attributes_array']['class'][$index]);
  }

  $vars['content_classes_array'][] = 'block-content';
}

/**
 * Implements hook_process_block().
 */
function vanilla_process_block(&$vars) {
  if (empty($vars['theme']['element'])) {
    $vars['theme']['element'] = 'div';
  }

  $vars['content_classes'] = implode(' ', $vars['content_classes_array']);
}

function vanilla_preprocess_pager_link(&$vars) {
	$vars['attributes']['class'][] = 'pager-nav-item-content';
}

function _vanilla_pager_nav($hook, $vars) {
	$class = $vars['class'];
	$text = $vars['text'];
	$element = $vars['element'];
	$interval = (isset($vars['interval'])) ? $vars['interval'] : NULL;
	$parameters = $vars['parameters'];

	$data = theme($hook, array(
		'text' => $text,
		'element' => $element,
		'interval' => $interval,
		'parameters' => $parameters
	));

	$class[] = 'pager-nav-item';

	if (empty($data)) {
		$class[] = 'pager-disabled';
		$data = "<span class='pager-nav-item-content'>$text</span>";
	}

  return array(
		'class' => $class,
		'data' => $data,
	);
}

function vanilla_pager($variables) {
  global $pager_page_array, $pager_total;

  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];

  $pager_middle = ceil($quantity / 2);
  $pager_current = $pager_page_array[$element] + 1;
  $pager_first = $pager_current - $pager_middle + 1;
  $pager_last = $pager_current + $quantity - $pager_middle;
  $pager_max = $pager_total[$element];

  $i = $pager_first;

  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }

  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }

  if ($pager_total[$element] > 1) {
    if ($pager_total[$element] > 2) {
      $items[] = _vanilla_pager_nav('pager_first', array(
				'class' => array('pager-first'),
				'text' => (isset($tags[0]) ? $tags[0] : t('« first')),
				'element' => $element,
				'parameters' => $parameters
			));
    }

    $items[] = _vanilla_pager_nav('pager_previous', array(
			'class' => array('pager-previous'),
			'text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')),
			'element' => $element,
			'interval' => 1,
			'parameters' => $parameters,
		));

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }

      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array(
							'text' => $i,
							'element' => $element,
							'interval' => ($pager_current - $i),
							'parameters' => $parameters,
						)),
          );
        }
        else if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-item', 'pager-current', 'pager-current-page'),
            'data' => "<span>$i</span>",
          );
        }
        else {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array(
							'text' => $i,
							'element' => $element,
							'interval' => ($i - $pager_current),
							'parameters' => $parameters,
						)),
          );
        }
      }

      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }

    $items[] = _vanilla_pager_nav('pager_next', array(
      'class' => array('pager-next'),
			'text' => (isset($tags[3]) ? $tags[3] : t('next ›')),
			'element' => $element,
			'interval' => 1,
			'parameters' => $parameters,
		));

    if ($pager_total[$element] > 2) {
      $items[] = _vanilla_pager_nav('pager_last', array(
        'class' => array('pager-last'),
				'text' => (isset($tags[4]) ? $tags[4] : t('last »')),
				'element' => $element,
				'parameters' => $parameters,
			));
    }

    return '<h2 class="element-invisible">' . t('Pages') . '</h2>' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager')),
    ));
  }
}

function vanilla_views_mini_pager($vars) {
  global $pager_page_array, $pager_total;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];

  $pager_current = $pager_page_array[$element] + 1;
  $pager_max = $pager_total[$element];

  if ($pager_total[$element] > 1) {
    $items[] = _vanilla_pager_nav('pager_previous', array(
      'class' => array('pager-previous'),
			'text' => (isset($tags[1]) ? $tags[1] : t('‹‹')),
			'element' => $element,
			'interval' => 1,
			'parameters' => $parameters,
		));

    $items[] = array(
      'class' => array('pager-item pager-current'),
      'data' => "<span class='pager-current-part pager-current-page'>$pager_current</span>" .
				"&nbsp;" .
				"<span class='pager-current-part'>of</span>" .
				"&nbsp;" .
				"<span class='pager-current-part pager-current-max'>$pager_max</span>",
    );

    $items[] = _vanilla_pager_nav('pager_next', array(
      'class' => array('pager-next'),
			'text' => (isset($tags[3]) ? $tags[3] : t('››')),
			'element' => $element,
			'interval' => 1,
			'parameters' => $parameters,
		));

    return theme('item_list', array(
      'items' => $items,
      'title' => NULL,
      'type' => 'ul',
      'attributes' => array('class' => array('pager', 'pager-mini')),
    ));
  }
}

/**
 * Implements hook_page_alter().
 */
function vanilla_page_alter(&$page) {
  $skip_link_anchor = theme_get_setting('zen_skip_link_anchor');
  $skip_link_text   = theme_get_setting('zen_skip_link_text');

  if ($skip_link_text && $skip_link_anchor) {
    $page['page_top']['vanilla_skip_link'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'skip-link'),
      '#weight' => -9999,
      '#children' => l($skip_link_text, '', array(
        'fragment' => $skip_link_anchor,
        'attributes' => array(
          'class' => array(
            'element-invisible',
            'element-focusable',
          ),
        ),
      )),
    );
  }
}

/**
 * Implements hook_html_head_alter().
 */
function vanilla_html_head_alter(&$elements) {
  // Add IE X-UA-Compatible tag, forcing latest version rendering.
  $elements['vanilla_xua'] = array(
    '#type' => 'html_tag',
    '#tag'  => 'meta',
    '#weight' => -10,
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content'  => 'IE=edge,chrome=1',
    ),
  );

  // Add cleartype.
  $elements['vanilla_cleartype'] = array(
    '#type'   => 'html_tag',
    '#tag'  => 'meta',
    '#weight' => -9,
    '#attributes' => array(
      'http-equiv' => 'cleartype',
      'content'  => 'on',
    ),
  );

  // Get mobile metatag settings.
  $html5_respond_meta = array_filter((array) theme_get_setting('zen_html5_respond_meta'));
  if (in_array('meta', $html5_respond_meta)) {
    $elements['vanilla_mobile'] = array(
      '#type'   => 'html_tag',
      '#tag'  => 'meta',
      '#weight' => -5,
      '#attributes' => array(
        'name'  => 'MobileOptimized',
        'content' => 'width',
      ),
    );

    $elements['vanilla_handheld'] = array(
      '#type'   => 'html_tag',
      '#tag'  => 'meta',
      '#weight' => -5,
      '#attributes' => array(
        'name'  => 'HandheldFriendly',
        'content' => 'true',
      ),
    );

    $elements['vanilla_viewport'] = array(
      '#type'   => 'html_tag',
      '#tag'  => 'meta',
      '#weight' => -5,
      '#attributes' => array(
        'name'  => 'viewport',
        'content' => 'width=device-width',
      ),
    );
  }
}

/**
 * Implements template_preprocess_entity().
 */
function vanilla_preprocess_entity(&$vars, $hook) {
  global $theme;
  $function = $theme . '_preprocess_' . $vars['entity_type'];

  if (function_exists($function)) {
    $function($vars, $hook);
  }
}
