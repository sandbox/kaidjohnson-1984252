; Drupal.org release file.
api = 2
core = 7.x

; Core
projects[drupal][version] = 7.69

; Patches for Core
projects[drupal][patch][] = https://drupal.org/files/1074108-skip-profile-16-7.x-do-not-test.patch
# projects[drupal][patch][] = https://drupal.org/files/issues/D7-registry_rebuild_should_not_parse_same_file_twice-1470656-34.patch
projects[drupal][patch][] = https://drupal.org/files/issues/2019-12-18/Drupal-core--865536-263--brower-key-for-js-do-not-test.patch
projects[drupal][patch][] = https://drupal.org/files/issues/drupal-7.x-allow_profile_change_sys_req-1772316-28.patch
# projects[drupal][patch][] = https://drupal.org/files/issues/2245003-use-random-seed-instead-of-session-id-for-csrf-token-d7_2.patch
projects[drupal][patch][] = https://drupal.org/files/issues/2245003-use-random-seed-instead-of-session-id-for-csrf-token-d7_2.patch
projects[drupal][patch][] = https://drupal.org/files/issues/drupal7-fix-simpletest-https-471970-140-7.x.patch
projects[drupal][patch][] = https://drupal.org/files/menu-link-save-recursive-equality-1982992-1.patch
projects[drupal][patch][] = https://drupal.org/files/comment-disable-per-content-type-2085885-4.patch
projects[drupal][patch][] = https://drupal.org/files/issues/drupal-n1256368-91.patch
# projects[drupal][patch][] = https://drupal.org/files/issues/issue-2408321-support-rfc5785-35-D7.patch
# projects[drupal][patch][] = https://drupal.org/files/issues/ignore_front_end_vendor-2329453-111_0.patch
